package cs598wg.wrapper;

public class ProfilingWrapper{

    static{
        System.load("/home/tshull226/Documents/school/Gropp_CS598WG/project/code/c_code/jni_code/libProfilingWrapper.so");
    }

    private native static void triggerSDEStart();
    private native static void triggerSDEEnd();
    private native static void triggerPerfStart(String outputLoc, String configName, int warmup_time, int attach_time, int repeat_num);
    private native static void triggerSDEStartAttach(String outputLoc, int useFootprint, int useHistogram);

    public static void printMessage(){
        System.out.println("The java part seems to be working");
    }
    public static void startSDEWrapper(){
        System.out.println("starting the sde wrapper");
        triggerSDEStart();
    }
    public static void startSDEWrapperAttach(String outputLoc, int useFootprint, int useHistogram){
        System.out.println("starting the sde wrapper attach " + outputLoc + ", " + useFootprint + ", " + useHistogram);
        triggerSDEStartAttach(outputLoc, useFootprint, useHistogram);
    }
    public static void startPerfWrapper(String outputLoc, String configName, int warmup_time, int attach_time, int repeat_num){
        System.out.println("starting the perf wrapper");
        triggerPerfStart(outputLoc, configName, warmup_time, attach_time, repeat_num);
    }
    public static void endSDEWrapper(){
        System.out.println("ending the sde wrapper");
        triggerSDEEnd();
    }

}
