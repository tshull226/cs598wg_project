package cs598wg.nbody

import akka.actor.{Actor, ActorLogging, Props}
import scala.math.{pow, sqrt}

class Planet(xPos: Double, yPos: Double, zPos: Double, mass: Double) extends Actor with ActorLogging {
  import Planet._
  import ConfigInfo.timestep

  var position = Position(xPos,yPos,zPos)
  var velocity = Velocity(0,0,0)

  val name = "Planet"


  def processPlanets(planets: List[PlanetInfo]) : Unit = {
    def computeMagnitude(otherPosition: Position) = {
      val Position(x1, y1, z1) = position
      val Position(x2, y2, z2) = otherPosition
      val magnitude = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2) + pow(z2 - z1, 2))
      magnitude
    }

    def computeDistance(otherPosition: Position) = {
      val Position(x1, y1, z1) = position
      val Position(x2, y2, z2) = otherPosition
      //This one was wrong
      //(x2 - x1, y2 - y1, z2 - z1)
      (x1 - x2, y1 - y2, z1 - z2)
    }

    def addForce(forces: (Double, Double, Double), scalarAmount: Double, distance: (Double, Double, Double)) = {
      val (x1, y1, z1) = forces
      val (x2, y2, z2) = distance
      (x1 + scalarAmount*x2 , y1 + scalarAmount*y2, z1 + scalarAmount*z2)
    }

    def calculateNewVelocity(forces: (Double, Double, Double))= {
      val (fx, fy, fz) = forces
      val Velocity(vx, vy, vz) = velocity
      val ax = fx / mass
      val ay = fy / mass
      val az = fz / mass
      val newV = (vel: Double, acc: Double) => vel + acc * timestep
      Velocity(newV(vx, ax), newV(vy, ay), newV(vz, az))
    }

    def moveToNextPosition()= {
      val Velocity(vx, vy, vz) = velocity
      val Position(x, y, z) = position
      val newP = (pos: Double, vel: Double) => pos + vel * timestep
      Position(newP(x, vx), newP(y, vy), newP(z, vz))
    }

    // This is the forces for this iteration
    var forces : (Double, Double, Double) = (0,0,0)

    // first calculation the new force
    for(planet <- planets){
      val PlanetInfo(otherMass, otherPosition) = planet
      val magnitude = computeMagnitude(otherPosition)
      if(magnitude != 0){
        val scalarAmount = G * mass * otherMass / pow(magnitude, 3)
        val distance = computeDistance(otherPosition)
        forces = addForce(forces, scalarAmount, distance)
      }
    }
    // now the new velocity
    velocity = calculateNewVelocity(forces)

    // finally the new position
    position = moveToNextPosition()
  }


  def processMessages: Receive = {
    case ReturnPlanetInfo => 
      sender() ! PlanetInfo(mass, position)
    case AllPlanets(planets) =>
      processPlanets(planets)
      sender() ! SolarSystem.PlanetDone
  }

  def waitForPrint: Receive = {
    case PrintPlanetInfo =>
      printInfo()
  }

  def printInfo() = {
    val Position(xPos, yPos, zPos) = position
    val Velocity(xVel, yVel, zVel) = velocity
    log.info(s"this is a Planet currently at position ($xPos,$yPos,$zPos), with velocity ($xVel,$yVel,$zVel) and mass $mass")
  }

  def combiner(func: Receive) = func orElse waitForPrint

  def receive = combiner(processMessages)

}

object Planet {
  val G = 6.67300E-11
  case object ReturnPlanetInfo
  case object PrintPlanetInfo
}

  case class Position(xPos: Double, yPos: Double, zPos: Double)
  case class Velocity(xVel: Double, yVel: Double, zVel: Double)
  case class PlanetInfo(mass: Double, pos: Position)
