package cs598wg.nbody

import akka.actor.{ActorSystem, Actor, ActorLogging, Props, ActorRef}
import com.typesafe.config._
import scala.concurrent.duration._
import cs598wg.wrapper._

object ApplicationMain extends App {

  def handleArguments() = {
    def matchArgument(value: String){
      //FIXME need to add a couple more options here
      val branchFactor = """--branchFactor=(\d+)""".r
      val numSteps = """--numSteps=(\d+)""".r
      val numWarmups = """--numWarmups=(\d+)""".r
      val outputLoc = """--outputLoc=(.+)""".r
      val configLoc = """--configLoc=(.+)""".r
      val waitTime = """--waitTime=(\d+)""".r
      val runTime = """--runTime=(\d+)""".r
      val numRepeats = """--numRepeats=(\d+)""".r
      value match {
        case branchFactor(n) =>
          println(s"[ARGS] Setting the branching factor to ${n}")
          val num = n.toInt
          ConfigInfo.num_galaxies = num
          ConfigInfo.num_solar_systems = num
          ConfigInfo.num_planets = num
        case numSteps(n) =>
          val num = n.toDouble
          this.numSteps = num
        case numWarmups(n) =>
          val num = n.toDouble
          this.numWarmupSteps = num
        case outputLoc(dir) =>
          this.instrOutputLoc = dir
        case "--doFootprint" =>
          println("am doing the footprint")
          this.sdeDoFootprint = 1
        case "--doHistogram" =>
          this.sdeDoHistogram = 1
        case configLoc(dir) =>
          this.perfConfigLoc = dir
        case waitTime(time) =>
          this.perfWaitTime = time.toInt
        case runTime(time) =>
          this.perfRunTime = time.toInt
        case numRepeats(time) =>
          this.perfNumRepeats = time.toInt
      }
    }
    for(arg <- args){
      matchArgument(arg)
    }
  }

  def handleShutdown() = {
    config.getString("nbody.general.trigger-profiling") match {
      case "none" | "perf" =>
        //don't have to do anything
      case "sde" =>
        //now don't have to do anything
        //ProfilingWrapper.endSDEWrapper()
    }
    println(s"finish time of execution: ${System.currentTimeMillis()}")
    universe ! Universe.PrintNumSteps
    universe ! Universe.StopSimulation
    system.awaitTermination()
  }

  def handleWarmup() = {
    //val numStepsWarmup = config.getDouble("nbody.general.number-warmup-steps")

    println(s"performing ${numWarmupSteps} steps of warmup")
    val runner = system.actorOf(Props(new StepRunner(universe, numWarmupSteps)))
    runner ! StepRunner.StartWarmup
    do{
      Thread.sleep(waitTime)
    } while (!runner.isTerminated)
    println("the warmup has completed")
  }

  def handleInteractiveMode() = {
    //universe ! Universe.PrintUniverseInfo
    //universe ! Universe.StartSimulation
    var input = ""
    do{
      input = scala.io.StdIn.readLine("what do you want?\n")
      println(s"the input received is $input")
      input match {
        case "print" =>
          println("printing out info")
          universe ! Universe.PrintUniverseInfo
        case "step" =>
          println ("moving ahead a step")
          universe ! Universe.StartSimulation
        case "done" =>
          println("terminating execution")
        case _ =>
          println(s"unable to understand request: $input")

      }
    }while(input != "done");

    handleShutdown()
  }

  def handleStandaloneMode() = {
    universe ! Universe.StartSimulation

    import system.dispatcher
    import scala.language.postfixOps

    val time = config.getInt("nbody.general.length-of-computation")
    system.scheduler.scheduleOnce(time seconds){
      handleShutdown()
    }
  }

  def handleSetStepsMode() = {
    //val numSteps = config.getDouble("nbody.general.number-of-steps")
    //universe ! Universe.StartSimulation(numSteps)

    val runner = system.actorOf(Props(new StepRunner(universe, this.numSteps)))
    runner ! StepRunner.StartStep
    do{
      Thread.sleep(waitTime)
    } while (!runner.isTerminated)
    println("the handle set steps has finished")
    handleShutdown()
  }


  val waitTime = 1000

  val config = ConfigFactory.load()
  var numSteps = config.getDouble("nbody.general.number-of-steps")
  var numWarmupSteps = config.getDouble("nbody.general.number-warmup-steps")
  var instrOutputLoc = "none"
  var perfConfigLoc = "none"
  var sdeDoFootprint = 0
  var sdeDoHistogram = 0
  var perfWaitTime = 0
  var perfRunTime = 0
  var perfNumRepeats = 0

  //handling arguments after initially setting all of the variables
  handleArguments()

  //setting up the universe
  val system = ActorSystem("MyActorSystem")
  val universe = system.actorOf(Universe.props, "universe")

  if(config.getString("nbody.general.include-warmup") == "yes"){
    handleWarmup()
  }
  { //this is for attaching things onto the execution
    config.getString("nbody.general.trigger-profiling") match {
      case "none" =>
        //don't have to do anything
      case "sde" =>
        //ProfilingWrapper.startSDEWrapper()
        ProfilingWrapper.startSDEWrapperAttach(instrOutputLoc, sdeDoFootprint, sdeDoHistogram)
      case "perf" =>
        //val config_num = config.getInt("nbody.perf-profiling.config-num")
        //val attach_time = config.getInt("nbody.perf-profiling.attach-time")
        //val repeat_num = config.getInt("nbody.perf-profiling.repeat-num")
        ProfilingWrapper.startPerfWrapper(instrOutputLoc, perfConfigLoc, perfWaitTime, perfRunTime, perfNumRepeats)
    }
  }
  println(s"start time of execution: ${System.currentTimeMillis()}")
  val mode = config.getString("nbody.general.mode")
  mode match {
    case "interactive" =>
      handleInteractiveMode()
    case "standalone" =>
      handleStandaloneMode()
    case "set-steps" =>
      handleSetStepsMode()
  }

  class StepRunner(universe: ActorRef, numSteps: Double) extends Actor with ActorLogging {
    import StepRunner._
    def waitForStart: Receive = {
      case StartStep =>
        universe ! Universe.StartSimulation(numSteps)
        context.become(waitForEnd)
      case StartWarmup =>
        universe ! Universe.StartWarmup(numSteps)
        context.become(waitForEnd)
    }

    def waitForEnd: Receive = {
      case Universe.Done =>
        log.debug("received end message")
        context.stop(self)
    }

    def receive = waitForStart
  }
  object StepRunner {
    val props =Props[StepRunner]
    case object StartStep
    case object StartWarmup
  }
}
