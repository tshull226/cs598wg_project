package cs598wg.nbody

import com.typesafe.config._

object ConfigInfo {
  val config = ConfigFactory.load()
  val sharing_level =  config.getString("nbody.general.sharing-level") 
  val timestep = config.getDouble("nbody.general.timestep")
  val num_steps = config.getDouble("nbody.general.number-of-steps")
  val running_mode = config.getString("nbody.general.mode")

  val number_variation = config.getString("nbody.general.number.variation")
  val number_variance = config.getInt("nbody.general.number.variance")
  val number_min = config.getInt("nbody.general.number.min")
  val number_max = config.getInt("nbody.general.number.max")

  val planet_variation = config.getString("nbody.planet.mass.variation")
  val planet_norm = config.getDouble("nbody.planet.mass.norm")
  val planet_variance = config.getDouble("nbody.planet.mass.variance")
  val planet_min = config.getDouble("nbody.planet.mass.min")
  val planet_max = config.getDouble("nbody.planet.mass.max")

  val universe_combine = config.getString("nbody.universe.aggregate-option")
  val galaxy_combine = config.getString("nbody.galaxy.aggregate-option")
  val solar_system_combine = config.getString("nbody.solar-system.aggregate-option")

  var num_galaxies = config.getInt("nbody.galaxy.number")
  var num_solar_systems = config.getInt("nbody.solar-system.number")
  var num_planets = config.getInt("nbody.planet.number")

  val GalaxyStep = 1.0E10
  val GalaxySize = GalaxyStep / 2 // this is the radius of a galaxy
  //val GalaxySize = 5e19 // this is the radius of a galaxy
  // not using this value -- is dependent on the number of solar systems in the specific galaxy
  //val SolarSystemSize = 1e15
}

object Utils {
  //NOTE need to make sure this object can allow for multiple actors to access it concurrently
  import ConfigInfo._
  val randomFunc = scala.util.Random
  def getPosition(start: Position, stepSize: Double, number: Int) = {
    val Position(x, y, z) = start
    val posFunc = (pos: Double) => pos + stepSize * (number + 2.0)
    val posX = posFunc(x)
    val posY = posFunc(y)
    val posZ = posFunc(z)
    val pos = Position(posX, posY, posZ)
    pos
  }

  def getPosition(center: Position, radius: Double, number: Int, totalNum: Double) = {
    val Position(x, y, z) = center
    val diameter = radius * 2
    //want to leave a gap on either side
    val stepSize = diameter / (totalNum + 4.0)
    val posFunc = (pos: Double) => pos - radius + stepSize * (number + 2.0)
    val posX = posFunc(x)
    val posY = posFunc(y)
    val posZ = posFunc(z)
    val pos = Position(posX, posY, posZ)
    pos
  }

  def getNumber(system: String) = {
    def getGaussian(mean: Int, variance: Int) = {
      import scala.math.{sqrt, round, abs}
      val gaussianVal = sqrt(variance) * randomFunc.nextGaussian() + mean
      val value = abs(round(gaussianVal).toInt)
      value
    }
    def getRandom(min: Int, max: Int) = {
      val value = min + randomFunc.nextInt(max - min)
      value
    }
  
    var number = 0
    system match {
      case "galaxy" =>
        number = ConfigInfo.num_galaxies
      case "solar system" =>
        number = ConfigInfo.num_solar_systems
      case "planet" =>
        number = ConfigInfo.num_planets
    }
    number_variation match {
      case "none" =>
        //don't have to do anything
      case "gaussian" =>
        number = getGaussian(number, number_variance)
      case "random" =>
        number = getRandom(number_min, number_max)
    }
    number
  }
  import java.util.concurrent.atomic.AtomicInteger
  val atomicInt = new AtomicInteger(1)

  def getMass() = {
    def getGaussian(mean: Double, variance: Double) = {
      import scala.math.{sqrt, round, abs}
      val value = sqrt(variance) * randomFunc.nextGaussian() + mean
      value
    }
    def getRandom(min: Double, max: Double) = {
      import scala.math.{round}
      val value = min + randomFunc.nextDouble()*(max-min)
      value
    }
  
    var number = 0.0
    planet_variation match {
      case "none" =>
        number = planet_norm
      case "gaussian" =>
        number = getGaussian(planet_norm, planet_variance)
      case "random" =>
        number = getRandom(planet_min, planet_max)
      case "increasing" =>
        number = atomicInt.getAndIncrement()
    }
    number
  }

  def combinePlanets(planets: List[PlanetInfo], centerPos: Position, calculateCenter: Boolean): List[PlanetInfo] = {
    var totalMass = 0.0
    var newPosition = centerPos
    for(planet <- planets){
      val PlanetInfo(mass, _) = planet
      totalMass += mass
    }
    if(calculateCenter){
      var xPos = 0.0
      var yPos = 0.0
      var zPos = 0.0
      for(planet <- planets){
        val PlanetInfo(mass, Position(x, y, z)) = planet
        xPos += x * mass / totalMass
        yPos += y * mass / totalMass
        zPos += z * mass / totalMass
      }
      newPosition = Position(xPos, yPos, zPos)
    }
    List(PlanetInfo(totalMass, newPosition))
  }
}
