package cs598wg.nbody

import akka.actor.{Actor, ActorLogging, Props, ActorRef}
import com.typesafe.config._

class Universe extends Actor with ActorLogging {
  import Universe._

  val config = ConfigFactory.load()

  // am not using right now
  var numSteps = ConfigInfo.num_steps


  val numGalaxies = Utils.getNumber("galaxy")

  var initiatingActor: ActorRef = null
  var inWarmup = false

  /**
   * This method arranges the galaxies
   * I am going to different orders of magnitudes layout of different galaxies, solar systems, planets
   */
  def initializeGalaxies() {

    for(i <- 1 to numGalaxies){
      //added the plus one to make sure it stays inside the bounds of the solar system
      val galaxyPosition = Utils.getPosition(Position(0,0,0), ConfigInfo.GalaxyStep, i)
      val Position(x, y, z) = galaxyPosition
      context.actorOf(Props(new Galaxy(x, y, z, ConfigInfo.GalaxySize)))
    }
  }

  initializeGalaxies()
  /*
  for(i <- 1 to numGalaxies){
    val rad: Double = 1
    context.actorOf(Props(new Galaxy(i,i,i,rad)))
  }
  */

 val galaxies = context.children

 var stepsCompleted = 0

 //TODO need to use actor.become, make this more functional have the galaxies received, complete, etc as 
 //part of the functional state

 def waitForStart: Receive = {
   case StartSimulation =>
     initiatingActor = sender
     log.debug("In Universe - Starting Simulation")
     startStep()
   case StartSimulation(num) =>
     initiatingActor = sender
     log.debug("In Universe with Step Count - Starting Simulation")
     numSteps = num
     startStep()
   case StartWarmup(num) =>
     inWarmup = true
     initiatingActor = sender
     log.debug("In Universe with Step Count - Starting Warmup")
     numSteps = num
     startStep()
 }

 def waitForInfo(numReceived: Int, planetList: List[PlanetInfo]): Receive = {
   case GalaxyInfo(planets) =>
     val receivedCount = numReceived + 1
     val listOfPlanets = planets ++ planetList
     if(receivedCount == galaxies.size){
       //checking if they have been combined
       var returnedPlanets = listOfPlanets
       ConfigInfo.universe_combine match {
         case "none" =>
           returnedPlanets = listOfPlanets // don't have to change anything
         case "mass+pos" =>
           returnedPlanets = Utils.combinePlanets(listOfPlanets, Position(0,0,0), true)
       }
       galaxies foreach {galaxy =>
         galaxy ! AllPlanets(returnedPlanets)
       }
       context.become(combiner(waitForDone(0)))
     }else{
       context.become(combiner(waitForInfo(receivedCount, listOfPlanets)))
     }
 }

 def startStep() = {
   galaxies foreach {galaxy =>
     galaxy ! Galaxy.StartStep
   }
   ConfigInfo.sharing_level match{
     case "galaxy" | "solar system" =>
       context.become(combiner(waitForDone(0)))
     //case "solar system" =>
     //  context.become(combiner(waitForDone(0)))
     case "universe" =>
       context.become(combiner(waitForInfo(0, List[PlanetInfo]())))
   }
 }

 def waitForDone(numReceived: Int): Receive = {
   case GalaxyDone =>
     val count = numReceived + 1
     if(count == numGalaxies){
       //this step is done
       stepsCompleted += 1
       if(inWarmup){
         //println("Warmup Step Has Completed")
         if(stepsCompleted < numSteps){
           startStep()
         }else{
           inWarmup = false
           stepsCompleted = 0
           initiatingActor ! Done
           context.become(combiner(waitForStart))
         }
       }else{
         //println("Step Has Completed")
         val mode = ConfigInfo.running_mode
         mode match {
           case "standalone" =>
             startStep()
           case "interactive" =>
             context.become(combiner(waitForStart))
           case "set-steps" =>
             if(stepsCompleted < numSteps){
               startStep()
             }else{
               initiatingActor ! Done
               //context.system.shutdown()
               //need to do this now...
               context.become(combiner(waitForStart))
             }
         }
       }
     } else{
       context.become(combiner(waitForDone(count)))
     }
 }

 def waitForOther: Receive = {
   case PrintUniverseInfo =>
     printInfo()
     for(galaxy <- galaxies) {
       galaxy ! Galaxy.PrintGalaxyInfo
     }
   case StopSimulation =>
     context.system.shutdown()
   case PrintNumSteps =>
     println(s"Total Number of Steps Completed: ${stepsCompleted}")
 }

 def printInfo() = {
   log.info(s"This is a universe with ${galaxies.size} galaxies")
 }

 //def combiner(func: PartialFunction[Any, Unit]) = func orElse waitForPrint
 def combiner(func: Receive) = func orElse waitForOther

 def receive = combiner(waitForStart)

}

object Universe {
  val props = Props[Universe]
  case class GalaxyInfo(planets: List[PlanetInfo])
  case object GalaxyDone
  case object StartSimulation
  case class StartSimulation(numSteps: Double)
  case class StartWarmup(numSteps: Double)
  case object PrintNumSteps
  case object StopSimulation
  case object PrintUniverseInfo
  case object Done
}

class Galaxy(xPos: Double, yPos: Double, zPos: Double, radius: Double) extends Actor with ActorLogging {
  import Galaxy._

  val numSolarSystems = Utils.getNumber("solar system")
  val solarSystemSize = radius / (numSolarSystems + 2)

  var aggregatedValue:PlanetInfo = _
  var originalPlanetList = List[PlanetInfo]()

  def initializeSolarSystems() {

    for(i <- 1 to numSolarSystems){
      val solarSystemPosition = Utils.getPosition(Position(xPos, yPos, zPos), radius, i, numSolarSystems)
      val Position(x, y, z) = solarSystemPosition
      //context.actorOf(Props(new SolarSystem(x, y, z, ConfigInfo.SolarSystemSize)))
      context.actorOf(Props(new SolarSystem(x, y, z, solarSystemSize)))
    }
  }

  initializeSolarSystems()
  val solarSystems = context.children

  def waitForStep: Receive = {
    case StartStep =>
      log.debug("In Galaxy - Starting Simulation")
      solarSystems foreach {solarSystem =>
        solarSystem ! SolarSystem.StartStep
      }
      ConfigInfo.sharing_level match{
        case "galaxy" | "universe" =>
          context.become(combiner(waitForInfo(0, List[PlanetInfo]())))
        case "solar system" =>
          context.become(combiner(waitForDone(0)))
        //case "universe" =>
        //  context.become(combiner(waitForInfo(0, List[PlanetInfo]())))
      }
  }

  def waitForInfo(numReceived: Int, planetList: List[PlanetInfo]): Receive = {
    case SolarSystemInfo(planets) =>
      val receivedCount = numReceived + 1
      val listOfPlanets = planets ++ planetList
      if(receivedCount == solarSystems.size){
        //There is where we will either send it to the parent or just relay it to the children
        if(ConfigInfo.sharing_level == "galaxy"){
          self ! AllPlanets(listOfPlanets)
        } else {
          //need to potentially combine planets here
          var returnedPlanets: List[PlanetInfo] = listOfPlanets
          ConfigInfo.galaxy_combine match {
            case "none" =>
              returnedPlanets = listOfPlanets
            case "mass" =>
              originalPlanetList = listOfPlanets
              returnedPlanets = Utils.combinePlanets(listOfPlanets, Position(xPos, yPos, zPos), false)
              aggregatedValue = returnedPlanets.head
            case "mass+pos" =>
              originalPlanetList = listOfPlanets
              returnedPlanets = Utils.combinePlanets(listOfPlanets, Position(xPos, yPos, zPos), true)
              aggregatedValue = returnedPlanets.head
          }
          context.parent ! Universe.GalaxyInfo(returnedPlanets)
        }
        context.become(combiner(waitForList))
      }else{
        context.become(combiner(waitForInfo(receivedCount, listOfPlanets)))
      }
  }

  def waitForList: Receive = {
    case AllPlanets(planetList) =>
      //need to potentially combine planets here
      var returnedPlanets: List[PlanetInfo] = planetList
      ConfigInfo.galaxy_combine match {
        case "none" =>
          returnedPlanets = planetList
        case "mass" | "mass+pos" =>
          val PlanetInfo(mass, position) = aggregatedValue
          returnedPlanets = returnedPlanets ++ originalPlanetList ++ List(PlanetInfo(-mass, position))
      }
      solarSystems foreach {solarSystem =>
        solarSystem ! AllPlanets(returnedPlanets)
        context.become(combiner(waitForDone(0)))
      }
  }

  def waitForDone(numReceived: Int): Receive = {
    case SolarSystemDone =>
      val count = numReceived + 1
      if(count == solarSystems.size){
        context.parent ! Universe.GalaxyDone
        context.become(combiner(waitForStep))
      }else{
        context.become(combiner(waitForDone(count)))
      }
  }

  def waitForPrint: Receive = {
    case PrintGalaxyInfo =>
      printInfo()
      for(solarSystem <- solarSystems) {
        solarSystem ! SolarSystem.PrintSolarSystemInfo
      }
  }

  def printInfo() = {
    log.info(s"this is a galaxy at position ($xPos,$yPos,$zPos), radius $radius, with ${solarSystems.size} solar systems")
  }

  def combiner(func: Receive) = func orElse waitForPrint

  def receive = combiner(waitForStep)

}

object Galaxy {
  case class SolarSystemInfo(planets: List[PlanetInfo])
  case object SolarSystemDone
  case object StartStep
  case object PrintGalaxyInfo
}

class SolarSystem(xPos: Double, yPos: Double, zPos: Double, radius: Double) extends Actor with ActorLogging {
  import SolarSystem._

  val numPlanets = Utils.getNumber("planet")

  var aggregatedValue:PlanetInfo = _
  var originalPlanetList = List[PlanetInfo]()

  def initializePlanets() {

    for(i <- 1 to numPlanets){
      val planetPosition = Utils.getPosition(Position(xPos, yPos, zPos), radius, i, numPlanets)
      val Position(x, y, z) = planetPosition
      val mass = Utils.getMass()
      context.actorOf(Props(new Planet(x, y, z, mass)))
    }
  }

  initializePlanets()

  val planets = context.children

  def waitForStep: Receive = {
    case StartStep =>
      log.debug("In Solar System - Starting Simulation")
      planets foreach {planet =>
        planet ! Planet.ReturnPlanetInfo
      }
      context.become(combiner(waitForInfo(0, List[PlanetInfo]())))
  }

  def waitForInfo(numReceived: Int, planetList: List[PlanetInfo]): Receive = {
    case PlanetInfo(mass, pos) =>
      val receivedCount = numReceived + 1
      val listOfPlanets = PlanetInfo(mass, pos) :: planetList
      if(receivedCount == planets.size){
        //There is where we will either send it to the parent or just relay it to the children
        if(ConfigInfo.sharing_level == "solar system"){
          self ! AllPlanets(listOfPlanets)
        } else {
          //need to potentially combine planets here
          var returnedPlanets: List[PlanetInfo] = listOfPlanets
          ConfigInfo.solar_system_combine match {
            case "none" =>
              returnedPlanets = listOfPlanets
            case "mass" =>
              originalPlanetList = listOfPlanets
              returnedPlanets = Utils.combinePlanets(listOfPlanets, Position(xPos, yPos, zPos), false)
              aggregatedValue = returnedPlanets.head
            case "mass+pos" =>
              originalPlanetList = listOfPlanets
              returnedPlanets = Utils.combinePlanets(listOfPlanets, Position(xPos, yPos, zPos), true)
              aggregatedValue = returnedPlanets.head
          }
          context.parent ! Galaxy.SolarSystemInfo(returnedPlanets)
        }
        context.become(combiner(waitForList))
      }else{
        context.become(combiner(waitForInfo(receivedCount, listOfPlanets)))
      }
  }

  def waitForList: Receive = {
    case AllPlanets(planetList) =>
      var returnedPlanets: List[PlanetInfo] = planetList
      ConfigInfo.solar_system_combine match {
        case "none" =>
          returnedPlanets = planetList
        case "mass" | "mass+pos" =>
          //FIXME this may have a problem depending on how position may be changed by the upper level -- I think it's good enough
          //especially b/c I don't particularly care about the accuracy -- I'm just trying to model the computation
          val PlanetInfo(mass, position) = aggregatedValue
          returnedPlanets = returnedPlanets ++ originalPlanetList ++ List(PlanetInfo(-mass, position))
      }
      planets foreach {planet =>
        planet ! AllPlanets(returnedPlanets)
        context.become(combiner(waitForDone(0)))
      }
  }

  def waitForDone(numReceived: Int): Receive = {
    case PlanetDone =>
      val count = numReceived + 1
      if(count == planets.size){
        context.parent ! Galaxy.SolarSystemDone
        context.become(combiner(waitForStep))
      }else {
        context.become(combiner(waitForDone(count)))
      }
  }

  def waitForPrint: Receive = {
    case PrintSolarSystemInfo =>
      printInfo()
      for(planet <- planets) {
        planet ! Planet.PrintPlanetInfo
      }
  }

  def printInfo() = {
    log.info(s"this is a solar system at position ($xPos,$yPos,$zPos), radius $radius, with ${planets.size} planets")
  }

  def combiner(func: Receive) = func orElse waitForPrint

  def receive = combiner(waitForStep)
}

object SolarSystem {
  case class PlanetList(planets: List[PlanetInfo])
  case object PlanetDone
  case object StartStep
  case object PrintSolarSystemInfo
}

  case class AllPlanets(planets: List[PlanetInfo])
