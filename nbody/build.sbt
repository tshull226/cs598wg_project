name := """nbody-app"""

version := "0.1"

scalaVersion := "2.11.6"

scalacOptions += "-feature"
scalacOptions += "-deprecation"

assemblyJarName in assembly := "nbody.jar"
assemblyOutputPath in assembly := file("../jars/nbody.jar")

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.3.11")
  //"com.typesafe.akka" %% "akka-testkit" % "2.3.11" % "test",
  //"org.scalatest" %% "scalatest" % "2.2.4" % "test")


connectInput in run := true

fork in run := true
