
/* Hello World program */

#include<stdio.h>
#include<unistd.h>
#include<signal.h>
#include <sys/types.h>

extern void pin_simulation_begin() __attribute__ ((noinline));
extern void pin_simulation_end() __attribute__ ((noinline));
extern void pin_simulation_exit() __attribute__ ((noinline));
void pin_simulation_begin() { asm(""); }
void pin_simulation_end() { asm(""); }
void pin_simulation_exit() { asm(""); }

void runCalc(){
    double sum = 0;
    int i;
    for(i = 0; i < 300; i++){
        sum += i;
    }
    printf("the sum is %f\n", sum);
}

void runSDETest(){
    //raise(SIGTRAP);
    //__asm__("int $3");
    pin_simulation_begin();
    runCalc();
    //raise(SIGINT);
    pin_simulation_end();
    //__asm__("int $3");
}

void runPerfTest(){
    char pid_string[10];
    sprintf(pid_string, "%ld", (long)getpid());
    printf("received number: %s\n", pid_string);
    int pid = fork();
    if(pid == 0) {
        //the child process
        printf("child number %ld\n", (long)getpid());
        char *const paramList[] = {
            "/usr/bin/perf",
            "stat",
            "-v",
            "-o",
            "perf-result.txt",
            "-r",
            "5",
            //"-d",
            //"1000",
            "-p",
            pid_string,
            "sleep",
            "2",
            NULL
        };
        execv("/usr/bin/perf", paramList);
    } else{
        //the parent process
        int count = 0;
        for(count = 0; count < 10; count++){
            sleep(2);
            printf("parent number %ld\n", (long)getpid());
            printf("parent running\n");
            runCalc();
        }
    }
}

int main()
{
    printf("Hello World\n");
    runSDETest();
    //runPerfTest();
    printf("finished running\n");

    return 0;
}
