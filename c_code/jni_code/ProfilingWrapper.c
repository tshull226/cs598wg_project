
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include "cs598wg_wrapper_ProfilingWrapper.h"

extern "C" void pin_simulation_begin() __attribute__ ((noinline));
extern "C" void pin_simulation_end() __attribute__ ((noinline));
extern "C" void pin_simulation_exit() __attribute__ ((noinline));
void pin_simulation_begin() { asm(""); }
void pin_simulation_end() { asm(""); }
void pin_simulation_exit() { asm(""); }

JNIEXPORT void JNICALL Java_cs598wg_wrapper_ProfilingWrapper_triggerSDEStart
  (JNIEnv *, jclass){
      printf("am about to trigger the sde begin\n");
      pin_simulation_begin();
  }

JNIEXPORT void JNICALL Java_cs598wg_wrapper_ProfilingWrapper_triggerSDEEnd
  (JNIEnv *, jclass){
      pin_simulation_end();
      printf("have triggered the sde end\n");
  }

char * getJavaString(JNIEnv *env, jstring orig){
    const char *javaString = env->GetStringUTFChars(orig, JNI_FALSE);
    //printf("this is the string found: %s\n", javaString);
    int str_size = 0;
    //str_size = strlen(javaString) + 1;
    while(javaString[str_size] != '\0'){
        str_size++;
    }
    str_size++;
    char *newString = (char *) malloc(sizeof(char) * str_size);
    //strcpy(newString, javaString);
    int i;
    for(i = 0; i < str_size; i++){
        newString[i] = javaString[i];
    }
    //printf("this is the string I created: %s\n", newString);
    env->ReleaseStringUTFChars(orig, javaString);
    return newString;
}

int readEventFile(char *location, char **params, int index){
    FILE *fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    fp = fopen(location, "r");
    if(fp == NULL){
        printf("PROBLEM: file was unable to be opened\n");
    }
    int str_size = 0;
    while((read = getline(&line, &len, fp)) != -1){
        printf("this is the line seen: %s", line);
        str_size = strlen(line) + 1;
        char *newString = (char *) malloc(sizeof(char) * str_size);
        strcpy(newString, line);
        newString[str_size - 2] = '\0';
        printf("this is the arg passed to perf: %s\n", newString);
        params[index++] = newString;
    }
    fclose(fp);
    return index;
}

JNIEXPORT void JNICALL Java_cs598wg_wrapper_ProfilingWrapper_triggerPerfStart
(JNIEnv *env, jclass, jstring outputLoc, jstring config_name, jint warmup_time, jint attach_time, jint repeat_num){
    char *perf_loc = "/usr/bin/perf";
    char *outputDir = getJavaString(env, outputLoc);
    char *configLoc = getJavaString(env, config_name);
    printf("intput received: %s, %s, %d, %d, %d\n", outputDir, configLoc, warmup_time, attach_time, repeat_num);
    char pid_string[10];
    sprintf(pid_string, "%ld", (long)getpid());
    printf("received number: %s\n", pid_string);
    int pid = fork();
    if(pid == 0) {
        //I'm going to sleep for a while here before actually launching perf...
        //the child process
        printf("going to sleep...\n");
        sleep(warmup_time);
        printf("child number %ld\n", (long)getpid());
        int paramLength = 100; //good enough (could be shortened)
        char **paramList = (char **) malloc(sizeof(char *) * paramLength);
        int index = 0;
        paramList[index++] = perf_loc;
        paramList[index++] = "stat";
        paramList[index++] = "--verbose";
        paramList[index++] = "--pid";
        paramList[index++] = pid_string;
        if(strcmp(outputDir, "none") != 0){
            paramList[index++] = "--output";
            char *newString = (char *) malloc(sizeof(char) * (strlen(outputDir) + 10));
            strcpy(newString, outputDir);
            strcat(newString, "/perf.out");
            paramList[index++] = newString;
        }
        if(strcmp(configLoc, "none") != 0){
            index = readEventFile(configLoc, paramList, index);
        }
        /*
        if(warmup_time > 0){
            char *warmupTime = (char *) malloc(sizeof(char) * 10);
            sprintf(warmupTime, "%d", (warmup_time*1000));
            paramList[index++] = "--delay";
            paramList[index++] = warmupTime;
        }
        */
        if(repeat_num > 1){
            char *repeatNum = (char *) malloc(sizeof(char) * 10);
            sprintf(repeatNum, "%d", repeat_num);
            paramList[index++] = "--repeat";
            paramList[index++] = repeatNum;
        }
        {//adding the command for how long to run perf
            char *attachTime = (char *) malloc(sizeof(char) * 10);
            sprintf(attachTime, "%d", attach_time);
            paramList[index++] = "sleep";
            paramList[index++] = attachTime;
        }
        paramList[index++] = NULL;
        execv(perf_loc, paramList);
    }
}


JNIEXPORT void JNICALL Java_cs598wg_wrapper_ProfilingWrapper_triggerSDEStartAttach
(JNIEnv *env, jclass, jstring outputLoc, jint doFootprint, jint doHistogram){
    char *sde_loc = "/home/tshull226/Documents/research/sde-external-7.45.0-2016-05-09-lin/sde";
    char *outputDir = getJavaString(env, outputLoc);
    char pid_string[10];
    sprintf(pid_string, "%ld", (long)getpid());
    printf("received number: %s\n", pid_string);
    int pid = fork();
    if(pid == 0) {
        //the child process
        printf("child number %ld\n", (long)getpid());
        int paramLength = 11;
        //paramLength = doFootprint? paramLength + 1 : paramLength;
        //paramLength = doHistogram? paramLength + 3 : paramLength;
        char **paramList = (char **) malloc(sizeof(char *) * paramLength);
        int index = 0;
        paramList[index++] = sde_loc;
        paramList[index++] = "-attach-pid";
        paramList[index++] = pid_string;
        if(strcmp(outputDir, "none") != 0){
            paramList[index++] = "-odir";
            paramList[index++] = outputDir;
        }
        if(doFootprint){
            paramList[index++] = "-footprint";
        }
        if(doHistogram){
            paramList[index++] = "-d";
            paramList[index++] = "-mix";
            paramList[index++] = "-mix_omit_per_function_stats";
            paramList[index++] = "-mix_omit_per_thread_stats";
        }
        paramList[index++] = NULL;
        //char *const paramList[] = {
        //    sde_loc,
        //    "-attach-pid",
        //    pid_string,
        //    "-footprint",
        //    NULL
        //};
        execv(sde_loc, paramList);
    }
    //parent doesn't have to do anything
}
