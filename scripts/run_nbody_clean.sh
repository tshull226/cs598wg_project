#!/bin/bash

#for factor in {2..30..2}
for factor in {31..40}
do
    DIR_NAME="initial_testing/${factor}"
    echo $DIR_NAME
    python benchmark_runner.py --benchmark nbody --branchingFactor ${factor} --logDir $DIR_NAME 
done
