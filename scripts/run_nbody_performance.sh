#!/bin/bash


declare -a WARMUP1=( 
'5944169' #2
'544141' #4
'49307' #6
'9152' #8
'2522' #10
'857' #12
'405' #14
'154' #16
)


declare -a WARMUP2=( 
'89' #18
'47' #20
'26' #22
'15' #24
'8' #26
'5' #28
'3' #30
'2' #32
'1' #34
'1' #36
'1' #38
'1' #40
)

declare -a NUMSTEP2=(
'896' #18
'476' #20
'268' #22
'158' #24
'82' #26
'52' #28
'40' #30
'27' #32
'19' #34
'16' #36
'9' #38
'8' #40
)

runTests(){
    count=0
    DIR_NAME_BASE="$1"
    for size in {2..16..2}
    do
        DIR_NAME="${DIR_NAME_BASE}/${size}"
        CONFIG_LOC="$2"
        NUM_WARMUPS=${WARMUP1[count]}
        echo $DIR_NAME
        python benchmark_runner.py --benchmark nbody --configLocation $CONFIG_LOC --branchingFactor ${size} --logDir $DIR_NAME  --numWarmups $NUM_WARMUPS
        count=$(( $count + 1 ))
    done

    count=0
    for size in {18..40..2}
    do
        NUM_WARMUPS=${WARMUP2[count]}
        NUM_STEPS=${NUMSTEP2[count]}
        DIR_NAME="${DIR_NAME_BASE}/${size}"
        CONFIG_LOC="$3"
        echo $DIR_NAME
        python benchmark_runner.py --benchmark nbody --configLocation $CONFIG_LOC --branchingFactor ${size} --logDir $DIR_NAME  --numSteps $NUM_STEPS --numWarmups $NUM_WARMUPS
        count=$(( $count + 1 ))
    done
}

CONFIG_DIR=/home/tshull226/Documents/school/Gropp_CS598WG/project/code/scripts/app_configs

#runTests baseline_test_1 "${CONFIG_DIR}/clean-small.conf" "${CONFIG_DIR}/clean-large.conf"
runTests baseline_test_2 "${CONFIG_DIR}/clean-small.conf" "${CONFIG_DIR}/clean-large.conf"
runTests baseline_test_3 "${CONFIG_DIR}/clean-small.conf" "${CONFIG_DIR}/clean-large.conf"
runTests solar_system_share_test "${CONFIG_DIR}/solar-system-share-small.conf" "${CONFIG_DIR}/solar-system-share-large.conf"
runTests galaxy_share_test "${CONFIG_DIR}/galaxy-share-small.conf" "${CONFIG_DIR}/galaxy-share-large.conf"

runTests opt1_test "${CONFIG_DIR}/opt1-small.conf" "${CONFIG_DIR}/opt1-large.conf"
runTests opt2_test "${CONFIG_DIR}/opt2-small.conf" "${CONFIG_DIR}/opt2-large.conf"
