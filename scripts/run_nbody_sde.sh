#!/bin/bash

#for factor in {2..30..2}
declare -a SIZES=( 
'2'
'5'
'10'
'15'
'20'
'25'
'30'
'35'
'40'
)

declare -a WARMUPS=( 
'1000'
'1000'
'100'
'25'
'5'
'2'
'2'
'2'
'2'
)

declare -a STEPS=( 
'1000'
'1000'
'100'
'25'
'5'
'2'
'2'
'2'
'2'
)

count=0
for size in "${SIZES[@]}"
do
    DIR_NAME="sde_testing/${size}"
    NUM_WARMUPS=${WARMUPS[count]}
    NUM_STEPS=${STEPS[count]}
    echo "dir name $DIR_NAME num steps $NUM_STEPS num warmups $NUM_WARMUPS"
    #python benchmark_runner.py --benchmark nbody --branchingFactor ${size} --logDir $DIR_NAME 
    #python benchmark_runner.py --benchmark nbody --branchingFactor ${size} --logDir $DIR_NAME  --numSteps $NUM_STEPS --numWarmups $NUM_WARMUPS 
    python benchmark_runner.py --benchmark nbody --branchingFactor ${size} --logDir $DIR_NAME  --numSteps $NUM_STEPS --numWarmups $NUM_WARMUPS --doFootprint --doHistogram
    count=$(( $count + 1 ))
done
