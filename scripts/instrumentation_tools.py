import argparse
from environmentParameters import *

class BaseInstrumentationFramework:
    def __init__(self):
        pass

    def parseArgs(self):
        raise NotImplementedError

    def getBaseCommand(self):
        raise NotImplementedError

    def getBenchmarkSpecificCommand(self, benchmark):
        raise NotImplementedError

class PerfInstrumentationFramework(BaseInstrumentationFramework):
    # think I am going to use perf-record and then perf-report
    # to read the results
    # there is some concept of an event filter -- need to
    # look into this
    def setInitialValues(self):
        self.events = []
        # self.events = ['ff','wer','bb']
        self.logDir = None

    def setupArgumentParser(self):
        parser = argparse.ArgumentParser(prog='Perf Instrumentation')
        #FIXME need to look at add_argument more I think right now
        # is can't share a starting letter with an argument in runner
        parser.add_argument('--pinDetailedCheck',
                           dest='pinDetailedCheck',
                           action='store_true',
                           default=False,
                           help='perform a detailed check')
        parser.add_argument('--logDir',
                           dest='logdir',
                           default=None,
                           help='the directory to log the results in')
        return parser

    def parseArgs(self):
        parser = self.setupArgumentParser()
        args, unknown = parser.parse_known_args()
        print "Perf Instrumentation Framework unknown args: ", unknown

        if args.logdir is not None:
            self.logDir = args.logdir


    def __init__(self):
        self.setInitialValues()

    def getBaseCommand(self):
        command = "perf-record"
        if len(self.events) != 0:
            eventString = ','.join(self.events)
            command = "%s --events=%s" % (command, eventString)
        return command

    def getBenchmarkSpecificCommand(self, benchmark):
        command = ""
        return command

class SDEInstrumentationFramework(BaseInstrumentationFramework):
    # think I am going to use perf-record and then perf-report
    # to read the results
    # there is some concept of an event filter -- need to
    # look into this
    def setInitialValues(self):
        self.sdeLocation = EnvironmentVariables.sdeLocation
        self.defaultArguments = EnvironmentVariables.sdeDefaultArgs
        self.doFootprint = False
        self.doHistogram = False
        self.logDir = None

    def setupArgumentParser(self):
        parser = argparse.ArgumentParser(prog='SDE Instrumentation')
        #FIXME need to look at add_argument more I think right now
        # is can't share a starting letter with an argument in runner
        parser.add_argument('--doHistogram',
                           dest='doHistogram',
                           action='store_true',
                           default=False,
                           help='perform a detailed check')
        parser.add_argument('--doFootprint',
                           dest='doFootprint',
                           action='store_true',
                           default=False,
                           help='perform a detailed check')
        parser.add_argument('--logDir',
                           dest='logdir',
                           default=None,
                           help='the directory to log the results in')
        return parser

    def parseArgs(self):
        parser = self.setupArgumentParser()
        args, unknown = parser.parse_known_args()
        print "Perf Instrumentation Framework unknown args: ", unknown
        if args.logdir is not None:
            self.logDir = args.logdir

        if args.doHistogram:
            self.doHistogram = True
        if args.doFootprint:
            self.doFootprint = True


    def __init__(self):
        self.setInitialValues()

    def getBaseCommand(self):
        command = "%s %s" % (self.sdeLocation, self.defaultArguments)
        if self.logDir is not None:
            command = "%s -odir %s" % (command, self.logDir)
        if self.doHistogram:
            command += " -d -mix -mix_omit_per_function_stats -mix_omit_per_thread_stats"
            #if self.logDir is not None:
            #    command = "%s -omix %s/sde-mix-out.txt" % (command, self.logDir)
        if self.doFootprint:
            command += " -footprint"
            #if self.logDir is not None:
            #    command = "%s -ofootprint %s/sde-footprint.txt" % (command, self.logDir)
        return command

    def getBenchmarkSpecificCommand(self, benchmark):
        command = ""
        return command


