#!/bin/bash


declare -a WARMUP1=( 
'5944169' #2
'544141' #4
'49307' #6
'9152' #8
'2522' #10
'857' #12
'405' #14
'154' #16
)


declare -a WARMUP2=( 
'89' #18
'47' #20
'26' #22
'15' #24
'8' #26
'5' #28
'3' #30
'2' #32
'1' #34
'1' #36
'1' #38
'1' #40
)

declare -a NUMSTEP2=(
'896' #18
'476' #20
'268' #22
'158' #24
'82' #26
'52' #28
'40' #30
'27' #32
'19' #34
'16' #36
'9' #38
'8' #40
)


NUM_REPEATS=5
WAIT_TIME=60
RUN_TIME=60

runTests(){
    count=0
    DIR_NAME_BASE="$1"
    PERF_CONFIG="$2"
    for size in {2..16..2}
    do
        DIR_NAME="${DIR_NAME_BASE}/${size}"
        CONFIG_LOC="$3"
        NUM_WARMUPS=${WARMUP1[count]}
        echo $DIR_NAME
        python benchmark_runner.py --benchmark nbody --perfConfig $PERF_CONFIG --configLocation $CONFIG_LOC --branchingFactor ${size} --logDir $DIR_NAME  --numWarmups $NUM_WARMUPS --waitTime $WAIT_TIME --runTime $RUN_TIME --numRepeats $NUM_REPEATS
        count=$(( $count + 1 ))
    done

    count=0
    for size in {18..40..2}
    do
        NUM_WARMUPS=${WARMUP2[count]}
        NUM_STEPS=${NUMSTEP2[count]}
        DIR_NAME="${DIR_NAME_BASE}/${size}"
        CONFIG_LOC="$4"
        echo $DIR_NAME
        python benchmark_runner.py --benchmark nbody --perfConfig $PERF_CONFIG --configLocation $CONFIG_LOC --branchingFactor ${size} --logDir $DIR_NAME  --numSteps $NUM_STEPS --numWarmups $NUM_WARMUPS --waitTime $WAIT_TIME --runTime $RUN_TIME --numRepeats $NUM_REPEATS

        count=$(( $count + 1 ))
    done
}

CONFIG_DIR=/home/tshull226/Documents/school/Gropp_CS598WG/project/code/scripts/app_configs
PERF_DIR=/home/tshull226/Documents/school/Gropp_CS598WG/project/code/scripts/perf_configs

#runTests perf_cpi_info "${PERF_DIR}/cpi_info.txt" "${CONFIG_DIR}/clean-small.conf" "${CONFIG_DIR}/clean-large.conf"
runTests perf_mem_info "${PERF_DIR}/mem_info.txt" "${CONFIG_DIR}/clean-small.conf" "${CONFIG_DIR}/clean-large.conf"
#runTests perf_thread_info "${PERF_DIR}/thread_info.txt" "${CONFIG_DIR}/clean-small.conf" "${CONFIG_DIR}/clean-large.conf"



