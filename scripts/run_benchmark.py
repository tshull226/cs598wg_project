#!/usr/local/bin/python

import argparse
import uuid
import os
import subprocess
import time
import sys

from benchmark_frameworks import *
from instrumentation_tools import *

#TODO really should add documentation do this
#performExecution = False
performExecution = True


class ScriptRunner:
    def setInitialValues(self):
        self.instrumentationFramework = None
        self.benchmarkFramework = None
        self.logDir = None

    def setupArgumentParser(self):
        parser = argparse.ArgumentParser(prog='Simulation Runner')
        groupInstr = parser.add_mutually_exclusive_group()
        groupInstr.add_argument('--usePerf',
                           dest='usePerf',
                           action='store_true',
                           default=False,
                           help='use perf as instrumentation')
        groupInstr.add_argument('--useSde',
                           dest='useSde',
                           action='store_true',
                           default=False,
                           help='use sde as instrumentation')
        parser.add_argument('--logDir',
                           dest='logdir',
                           default=None,
                           help='the directory to log the results in')
        parser.add_argument('--benchmark',
                           dest='benchmark',
                           default=None,
                           help='the benchmark framework to use')
        return parser

    def parseArgs(self):
        # FIXME this is temporary for right now
        # eventually want to parse the arguments for the info I want
        parser = self.setupArgumentParser()
        args, unknown = parser.parse_known_args()
        print "Script Runner unknown args: ", unknown

        frameworkMap = {
            "nbody": lambda: Nbody(),
            "fof": lambda: FriendOfFriends(),
        }

        if args.benchmark is not None:
            self.benchmarkFramework = frameworkMap[args.benchmark]()

        if args.logdir is not None:
            self.logDir = args.logdir


        if args.useSde:
            self.instrumentationFramework = SDEInstrumentationFramework()
        if args.usePerf:
            self.instrumentationFramework = PerfInstrumentationFramework()

        # need to have some check here to ensure this is set
        if self.benchmarkFramework is None:
            print "need to have a benchmark framework specified!"
            os._exit(-1)

    def parseFrameworkArgs(self):
        if self.instrumentationFramework is not None:
            self.instrumentationFramework.parseArgs()

        # don't really need to have this check (I'll check it earlier)
        self.benchmarkFramework.parseArgs()

    def runSetup(self):
        self.parseArgs()
        self.parseFrameworkArgs()


    def pauseForParallelRun(runningBenchmarks):
        emptySlots = False
        while not emptySlots:
            elementsToDelete = []
            # seeing if any child processes have completed
            for i in range(len(runningBenchmarks)):
                process = runningBenchmarks[i]
                if process.poll() is not None:
                    # this means this child process is done
                    print "process has finished"
                    elementsToDelete.append(i)
            # removing process from list
            elementsToDelete.reverse()
            for index in elementsToDelete:
                runningBenchmarks.pop(index)
            emptySlots = len(runningBenchmarks) < numParallel
            if not emptySlots:
                time.sleep(5)

    def getFileHandle(self, benchmark):
        output = sys.stdout
        if self.logDir is not None:
            logFile = "%s/stdout.log" % (self.logDir)
            output = open(logFile, 'w')
        return output

    def runBenchmarks(self):
        runningBenchmarks = []
        runParallel = False
        numParallel = 4

        instrumentationBaseCommand = ""
        if self.instrumentationFramework is not None:
            instrumentationBaseCommand = self.instrumentationFramework.getBaseCommand()

        benchmarks = self.benchmarkFramework.getBenchmarkList()
        #benchmarkBaseCommand = self.benchmarkFramework.getBaseCommand()

        for benchmark in benchmarks:
            if runParallel:
                pauseForParallelRun(runningBenchmarks)

            print "running benchmark ", benchmark

            instrumentationBenchmarkSpecificCommand = ""
            if self.instrumentationFramework is not None:
                instrumentationBenchmarkSpecificCommand = self.instrumentationFramework.getBenchmarkSpecificCommand(benchmark)
                # TODO might need a different separator for perf
                instrumentationBenchmarkSpecificCommand += " -- "
            benchmarkBenchmarkTotalCommand = self.benchmarkFramework.getBenchmarkTotalCommand(benchmark)

            # executing the command
            executionCommand = "%s %s %s" % (instrumentationBaseCommand,
                                     instrumentationBenchmarkSpecificCommand,
                                     benchmarkBenchmarkTotalCommand)

            print "execution command: ", executionCommand
            #temp so I don't have to keep commenting this out
            if performExecution:
                if runParallel:
                    # this should return immediately
                    process = subprocess.Popen(executionCommand, shell=True,
                                            stdout=self.getFileHandle(benchmark))
                    runningBenchmarks.append(process)
                else:
                    # pass
                    # this should wait until the call is finished before returning
                    subprocess.call(executionCommand, shell=True,
                                    stdout=self.getFileHandle(benchmark))

        # waiting here until all of the processes are finished executing
        if runParallel:
            for process in runningBenchmarks:
                # this waits until the process has finished
                process.wait()



    def __init__(self):
        self.setInitialValues()


def main():
    print "starting script"
    runner = ScriptRunner()
    runner.runSetup()
    runner.runBenchmarks()
    print "execution has finished"


if __name__ == '__main__':
    main()

