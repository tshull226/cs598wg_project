# LEGEND : 
#==========
# ST = lines (or pages) with STORE only
# LD = lines (or pages) with LOAD only
# CD = lines (or pages) with CODE only
# LD+ST = lines (or pages) with LOAD and SOTRE
# CD+LD = lines (or pages) with CODE and LOAD
# CD+ST = lines (or pages) with CODE and STORE
# C+L+S = lines (or pages) with CODE, LOAD, and STORE
# NEW = new accesses within interval
#                          ============================================= CACHE LINES =============================================
#  PERIOD[ms]          TID         LOAD        STORE        LD+ST         CODE        CD+LD        CD+ST        C+L+S          NEW 
# THREAD_START 0
# THREAD_START 1
# THREAD_START 2
# THREAD_START 3
# THREAD_START 4
# THREAD_START 5
# THREAD_START 6
# THREAD_START 7
# THREAD_START 8
# THREAD_START 9
# THREAD_START 10
# THREAD_START 11
# THREAD_START 12
# THREAD_START 13
# THREAD_START 14
# THREAD_START 15
# THREAD_START 16
# THREAD_START 17
# THREAD_START 18
# THREAD_START 19
# THREAD_START 20
# THREAD_START 21
# THREAD_START 22
# THREAD_START 23
# THREAD_START 24
# THREAD_START 25
# THREAD_START 26
# THREAD_START 27
# THREAD_START 28
# THREAD_START 29
# THREAD_START 30
# THREAD_START 31
# THREAD_START 32
# THREAD_START 33
# THREAD_START 34
# THREAD_START 35
# THREAD_START 36
# THREAD_START 37
# THREAD_START 38
# THREAD_START 39
# THREAD_START 40
# THREAD_START 41
# THREAD_START 42
# THREAD_START 43
          --           43         4118         2285         1747         2626            3            2            7        10788
          --           42       340677        32541      6077701         3867            9            0            4      6454799
          --           41          318           13          770          321            0            0            0         1422
          --           40       344502        33373      6302184         7163           12            0            0      6687234
          --           39          219           75          110          599            0            0            0         1003
          --           38       340597        32193      6010240         4027           10            1           11      6387079
          --           37         6930         1441         4198         6375            3            0            4        18951
          --           36       341330        32445      6057146         4416           10            1            1      6435349
          --           35       341115        33047      6319051         4183           11            0            3      6697410
          --           34       346529        33514      6106226         7736           18            3           19      6494045
          --           33       338147        32658      6221105         4428           13            2            5      6596358
          --           32         9067         1947        73062         7222            4            0           22        91324
          --           31            0            0            0            1            0            0            0            1
          --           30        10668           50         2314          920            0            0            0        13952
          --           29       339255        32666      6216553         3752           14            2            2      6592244
          --           28       340508        32619      6158944         4714            9            1            4      6536799
          --           27       340724        32465      6106623         4404            8            1            3      6484228
          --           26        12837        12242       114158        17542            0            0            0       156779
          --           25          162           49           73          393            0            0            0          677
          --           24        10239           45         1729          678            0            0            0        12691
          --           23       337159        32845      6339064         3789           10            0            0      6712867
          --           22       338377        32647      6244428         4204           15            0            1      6619672
          --           21        12754           53         2372          657            0            0            0        15836
          --           20          196          107           75          322            0            0            0          700
          --           19        11076           42         1974          628            0            0            0        13720
          --           18       338629        32251      6197096         4099           14            0            1      6572090
          --           17        20496         2869         8685         1689            0            0            0        33739
          --           16        11306           43         1001          674            0            0            0        13024
          --           15        13729           42          873          655            0            0            0        15299
          --           14        11097           37         1957          572            0            0            0        13663
          --           13        13353           58         1782          644            0            0            0        15837
          --           12       339295        32395      6164349         4389           10            1            4      6540443
          --           11       339415        32630      6217153         3973           13            2            6      6593192
          --           10       339866        32454      6145970         3916           10            0            2      6522218
          --            9       347251        32761      6037246         7434           16            0            3      6424711
          --            8       342869        33228      6232241         7157           23            2            4      6615524
          --            7        11250        14761       126436        16030            0            0            0       168477
          --            6       337205        32752      6210527         3984           11            0            0      6584479
          --            5       347987        33778      6192102         7838           23            2           26      6581756
          --            4       339016        32683      6264928         4161           12            1            5      6640806
          --            3       338942        32774      6223238         4135           15            0            0      6599104
          --            2       343591        33040      6127635         7342           13            6           16      6511643
          --            1       337676        32543      6210834         3520            9            0            6      6584588
          --            0          763           29          188          408            0            0            0         1388
          --          all        61494       138449     43568697        28061          166            1          437     43797305

# EOF
