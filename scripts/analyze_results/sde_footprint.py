import argparse
import re
import glob
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

data = dict()
args = None
originalDir = os.getcwd()

cat_names =[ 'LOAD', 'STORE', 'LD+ST', 'CODE', 'CD+LD', 'CD+ST', 'C+L+S', 'SUM']
footprintInfo = re.compile('\s+--\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)')
totalFootprintInfo = re.compile('\s+--\s+all\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)')

def writeResults():
    f = sys.stdout

    f.write('n, numPlanets')
    for name in cat_names:
        f.write("," + name)
    f.write('\n')
    for n in sorted(data):
        info = data[n]
        numPlanets = pow(n,3)
        f.write("%d,%d" % (n,numPlanets))
        values = info['list']
        for i in range(8):
            value = values[i]
            f.write(",%d" % value)
        f.write("\n")



def tryMatch(line, regex, dictionary):
    result = regex.match(line)
    if result:
        print "found match"
        info = []
        for i in range(1,9):
            value = long(result.group(i))
            info.append(value)
        dictionary['list'] = info
        print sum(info[0:7]), "    ", info[7]



def parseLine(line, dictionary):
    #tryMatch(line, footprintInfo, dictionary)
    tryMatch(line, totalFootprintInfo, dictionary)

def parseFile(dictionary):
    with open("sde-footprint.txt") as f:
        for line in f:
            #print line
#TID, LOAD, STORE, LD+ST, CODE, CD+LD, CD+ST, C+L+S, NEW
            parseLine(line, dictionary)



def parseArgs():
    global args
    parser = argparse.ArgumentParser(prog='SDE Instruction Mix')
    parser.add_argument('--folder', dest='folder', help='example')
    parser.add_argument('--output', dest='output', help='output name')
    parser.add_argument('--colors', dest='colors', help='example')
    args = parser.parse_args()


def main():
    global data
    parseArgs()
    os.chdir(args.folder)
    newDir = os.getcwd()
    for folder in glob.glob("*"):
        os.chdir(folder)
        value = int(folder)
        print "folder name ", folder
        data[value] = dict()
        parseFile(data[value])
        os.chdir(newDir)

    print data
    writeResults()



if __name__ == '__main__':
    main()
