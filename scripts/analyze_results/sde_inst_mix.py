import argparse
import re
import glob
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

data = dict()
args = None
originalDir = os.getcwd()

poundExpr = re.compile('\*(\S+)\s+(\d+)')
categoryExpr = re.compile('\*category-(\S+)\s+(\d+)')
instrExpr = re.compile('(\w+)\s+(\d+)')
numStepsExecuted = re.compile('Total Number of Steps Completed: (\d+)')

def writeResults():
    f = sys.stdout
    first = True
    categories = []
    f.write("\n\nabsolute values\n")
    for n in sorted(data):
        info = data[n]
        #writing the legend this way
        if first:
            first = False
            f.write("n, num planets")
            categories = sorted(info)
            for category in sorted(info):
                f.write(",%s" %(category))
            f.write('\n')

        numPlanets = pow(n,3)
        f.write("%d,%d" % (n,numPlanets))
        for category in categories:
            value = info[category]
            f.write(",%d" % value)
        f.write("\n")

    first = True
    f.write("\n\nnormalized values\n")
    for n in sorted(data):
        info = data[n]
        #writing the legend this way
        if first:
            first = False
            f.write("n, num planets")
            categories = sorted(info)
            for category in sorted(info):
                f.write(",%s" %(category))
            f.write('\n')

        numPlanets = pow(n,3)
        f.write("%d,%d" % (n,numPlanets))
        total = 0.0
        for category in categories:
            value = float(info[category])
            total += value
        for category in categories:
            value = float(info[category]) / total
            f.write(",%f" % value)
        f.write("\n")


"""
    f.write("n,num planets,num steps,time,planets per second,time per step, step per minute\n")
    for n in sorted(data):
        info = data[n]
        numPlanets = pow(n,3)
        steps = info["numSteps"]
        time = float(info["endTime"] - info["startTime"]) / 1000
        planetsPerSecond = float(steps) * numPlanets / time
        timePerStep = time/steps
        stepsPerMinute = 60 * steps/time
        output = "%d,%d,%d,%f,%f,%f,%f" % (n, numPlanets, steps, time, planetsPerSecond, timePerStep, stepsPerMinute)
        f.write(output)
        f.write("\n")
    for n in sorted(data):
        info = data[n]
        numPlanets = pow(n,3)
        steps = info["numSteps"]
        time = float(info["endTime"] - info["startTime"]) / 1000
        planetsPerSecond = float(steps) * numPlanets / time
        timePerStep = time/steps
        stepsPerMinute = 120 * steps/time
        output = "'%d' #%d" % (steps, n)
        f.write(output)
        f.write("\n")
"""


def tryMatch(line, regex, dictionary):
    result = regex.match(line)
    if result:
        dictionary[result.group(1)] = long(result.group(2))

def parseLine(line, dictionary):
    #tryMatch(line, poundExpr, dictionary)
    #tryMatch(line, instrExpr, dictionary)
    tryMatch(line, categoryExpr, dictionary)

def parseFile(dictionary):
    with open("sde-mix-out.txt") as f:
        for line in f:
            #print line
            parseLine(line, dictionary)



def parseArgs():
    global args
    parser = argparse.ArgumentParser(prog='SDE Instruction Mix')
    parser.add_argument('--folder', dest='folder', help='example')
    parser.add_argument('--output', dest='output', help='output name')
    parser.add_argument('--colors', dest='colors', help='example')
    args = parser.parse_args()


def main():
    global data
    parseArgs()
    os.chdir(args.folder)
    newDir = os.getcwd()
    for folder in glob.glob("*"):
        os.chdir(folder)
        value = int(folder)
        print "folder name ", folder
        data[value] = dict()
        parseFile(data[value])
        os.chdir(newDir)

    print data
    writeResults()



if __name__ == '__main__':
    main()
