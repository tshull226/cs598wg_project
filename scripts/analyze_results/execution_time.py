import argparse
import re
import glob
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

data = dict()
args = None
originalDir = os.getcwd()

executionStartTime = re.compile('start time of execution: (\d+)')
executionEndTime = re.compile('finish time of execution: (\d+)')
numStepsExecuted = re.compile('Total Number of Steps Completed: (\d+)')

def writeResults():
    f = sys.stdout

    f.write("n,num planets,num steps,time (sec),planets per second,planets per minute,time per step (sec),steps per minute\n")
    for n in sorted(data):
        info = data[n]
        numPlanets = pow(n,3)
        steps = info["numSteps"]
        time = float(info["endTime"] - info["startTime"]) / 1000
        planetsPerSecond = float(steps) * numPlanets / time
        planetsPerMinute = 60 * float(steps) * numPlanets / time
        timePerStep = time/steps
        stepsPerMinute = 60 * steps/time
        output = "%d,%d,%d,%f,%f,%f,%f,%f" % (n, numPlanets, steps, time, planetsPerSecond, planetsPerMinute, timePerStep, stepsPerMinute)
        f.write(output)
        f.write("\n")
    """
    for n in sorted(data):
        info = data[n]
        numPlanets = pow(n,3)
        steps = info["numSteps"]
        time = float(info["endTime"] - info["startTime"]) / 1000
        planetsPerSecond = float(steps) * numPlanets / time
        timePerStep = time/steps
        stepsPerMinute = 120 * steps/time output = "'%d' #%d" % (steps, n)
        f.write(output)
        f.write("\n")
    """



def tryMatch(line, regex, dictionary, key):
    result = regex.match(line)
    if result:
        dictionary[key] = int(result.group(1))

def parseLine(line, dictionary):
    tryMatch(line, executionStartTime, dictionary, 'startTime')
    tryMatch(line, executionEndTime, dictionary, 'endTime')
    tryMatch(line, numStepsExecuted, dictionary, 'numSteps')

def parseFile(dictionary):
    with open("stdout.log") as f:
        for line in f:
            #print line
            parseLine(line, dictionary)



def parseArgs():
    global args
    parser = argparse.ArgumentParser(prog='Blacklist Analysis')
    parser.add_argument('--folder', dest='folder', help='example')
    parser.add_argument('--output', dest='output', help='output name')
    parser.add_argument('--colors', dest='colors', help='example')
    args = parser.parse_args()


def main():
    global data
    parseArgs()
    os.chdir(args.folder)
    newDir = os.getcwd()
    for folder in glob.glob("*"):
        os.chdir(folder)
        value = int(folder)
        #print "folder name ", folder
        data[value] = dict()
        parseFile(data[value])
        os.chdir(newDir)

    writeResults()



if __name__ == '__main__':
    main()
