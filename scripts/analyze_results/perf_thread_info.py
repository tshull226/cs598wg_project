import argparse
import re
import glob
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

data = dict()
args = None
originalDir = os.getcwd()

perf_info = re.compile('(.+): (\d+) (\d+) (\d+)')

categories =[
    "context-switches",
    "page-faults",
    "cpu-migrations",
    "major-faults",
    "minor-faults",
    "task-clock",
]

def writeResults():
    f = sys.stdout

    f.write("n,num")
    for category in categories:
        f.write(",%s" % category)
    f.write('\n')
    for n in sorted(data):
        info = data[n]
        numPlanets = pow(n,3)
        f.write("%d,%d" % (n,numPlanets))
        for category in categories:
            values = info[category]
            count = float(len(values))
            value = sum(values) / count
            f.write(",%f" % value)
        f.write('\n')


def addResult(dictionary, key, value):
    if key not in dictionary:
        dictionary[key] = []
    dictionary[key].append(value)

def tryMatch(line, regex, dictionary):
    result = regex.match(line)
    if result:
        category = result.group(1)
        value = float(result.group(2))
        num = float(result.group(3))
        denom = float(result.group(4))
        normalized_result = value / (num/denom)
        addResult(dictionary, result.group(1), normalized_result)
        #dictionary[result.group(1)] = normalized_result

def parseLine(line, dictionary):
    tryMatch(line, perf_info, dictionary)

def parseFile(dictionary):
    with open("perf.out") as f:
        for line in f:
            #print line
            parseLine(line, dictionary)



def parseArgs():
    global args
    parser = argparse.ArgumentParser(prog='SDE Instruction Mix')
    parser.add_argument('--folder', dest='folder', help='example')
    parser.add_argument('--output', dest='output', help='output name')
    parser.add_argument('--colors', dest='colors', help='example')
    args = parser.parse_args()


def main():
    global data
    parseArgs()
    os.chdir(args.folder)
    newDir = os.getcwd()
    for folder in glob.glob("*"):
        os.chdir(folder)
        value = int(folder)
        print "folder name ", folder
        data[value] = dict()
        parseFile(data[value])
        os.chdir(newDir)

    print data
    writeResults()



if __name__ == '__main__':
    main()
