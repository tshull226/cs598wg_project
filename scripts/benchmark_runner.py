#!/usr/local/bin/python

import argparse
import uuid
import os
import subprocess
import time
import sys
import shutil

logDir = None

def run(arguments):
    executionCommand = "python run_benchmark.py %s" % (arguments)
    print "execution command ", executionCommand
    subprocess.call(executionCommand, shell=True)

def createArguments():
    real_args = sys.argv[1:]
    result = " ".join(real_args)
    return result


def runBenchmarks():
    createLogDir()
    arguments = createArguments()
    run(arguments)

def moveDirToCopy(dirName):
    num = 1
    newName = dirName + "-copy-" + str(num)
    while os.path.exists(newName):
        num += 1
        newName = dirName + "-copy-" + str(num)
    os.makedirs(newName)
    shutil.move(dirName, newName)
    print "moved dir to ", newName

def createLogDir():
    if logDir is None:
        return
    if(os.path.exists(logDir)):
        # have to copy this info elsewhere
        print "directory already exists, am moving it to backup"
        moveDirToCopy(logDir)
    os.makedirs(logDir)

def performSetup():
    global logDir
    def parseArgs():
        parser = argparse.ArgumentParser(prog='Benchmark Runner')
        parser.add_argument('--logDir',
                           dest='logDir',
                           default=None,
                           help='the log dir')
        args, unknown = parser.parse_known_args()
        print "Benchmark Runnger unknown args: ", unknown
        return args

    args = parseArgs()
    if args.logDir is not None:
        logDir = args.logDir
        print "log dir name ", logDir



def main():
    print "running benchmark runner"
    print "all args: " + str(sys.argv)
    performSetup()
    runBenchmarks()
    print "benchmark runner has finished"


if __name__ == '__main__':
    main()
