import argparse
import uuid
from environmentParameters import *

class BaseBenchmarkFramework:
    def __init__(self):
        pass

    def parseArgs(self):
        raise NotImplementedError

    def getBenchmarkList(self):
        raise NotImplementedError

    def getBaseCommand(self):
        raise NotImplementedError

    def getBenchmarkSpecificCommand(self, benchmark):
        raise NotImplementedError

    def getBenchmarkTotalCommand(self,benchmark):
        raise NotImplementedError

class Nbody(BaseBenchmarkFramework):
    def setInitialValues(self):
        self.configLocation = None
        self.javaLocation = EnvironmentVariables.javaLocation
        self.jarLocation = EnvironmentVariables.jarLocation
        self.branchingFactor = 0
        self.numSteps = 0
        self.numWarmups = 0
        self.doHistogram = None
        self.doFootprint = None
        self.logDir = None
        self.waitTime = None
        self.runTime = None
        self.numRepeats = None
        self.perfConfig = None

    def __init__(self):
        self.setInitialValues()

    def setupArgumentParser(self):
        parser = argparse.ArgumentParser(prog='Nbody Instrumentation')
        parser.add_argument('--configLocation',
                           dest='configLocation',
                           default=None,
                           help='where the config to use is located')
        parser.add_argument('--branchingFactor',
                           dest='branchingFactor',
                           default=None,
                           help='the degree of branching for the universe')
        parser.add_argument('--numSteps',
                           dest='numSteps',
                           default=None,
                           help='the number of steps to execute')
        parser.add_argument('--logDir',
                           dest='logDir',
                           default=None,
                           help='the number of warmup steps to execute')
        parser.add_argument('--numWarmups',
                           dest='numWarmups',
                           default=None,
                           help='the number of warmup steps to execute')
        parser.add_argument('--doHistogram',
                           dest='doHistogram',
                           action='store_true',
                           default=False,
                           help='use sde as instrumentation')
        parser.add_argument('--doFootprint',
                           dest='doFootprint',
                           action='store_true',
                           default=False,
                           help='use sde as instrumentation')
        parser.add_argument('--waitTime',
                           dest='waitTime',
                           default=None,
                           help='the time to wait before attaching perf')
        parser.add_argument('--runTime',
                           dest='runTime',
                           default=None,
                           help='the amount of time for each perf execution')
        parser.add_argument('--numRepeats',
                           dest='numRepeats',
                           default=None,
                           help='the number of samples for the perf execution')
        parser.add_argument('--perfConfig',
                           dest='perfConfig',
                           default=None,
                           help='the events to include in perf')
        return parser

    def parseArgs(self):
        parser = self.setupArgumentParser()
        args, unknown = parser.parse_known_args()
        print "Nbody Framework unknown args: ", unknown
        if args.branchingFactor is not None:
            self.branchingFactor = int(args.branchingFactor)
        if args.numSteps is not None:
            self.numSteps = int(args.numSteps)
        if args.numWarmups is not None:
            self.numWarmups = int(args.numWarmups)
        self.configLocation = args.configLocation
        self.doHistogram = args.doHistogram
        self.doFootprint = args.doFootprint
        self.logDir = args.logDir
        self.waitTime = args.waitTime
        self.runTime = args.runTime
        self.numRepeats = args.numRepeats
        self.perfConfig = args.perfConfig

    def getBenchmarkList(self):
        benchmarks = [
            "nbody"
        ]
        return benchmarks

    def getBaseCommand(self):
        command = self.javaLocation
        if self.configLocation is not None:
            command = "%s -Dconfig.file=%s" % (command, self.configLocation)
        command = "%s -jar %s/nbody.jar" % (command, self.jarLocation)
        if self.branchingFactor != 0:
            bf = self.branchingFactor
            command += (" --branchFactor=%s") % (bf)
        if self.numSteps != 0:
            command += (" --numSteps=%s") % (self.numSteps)
        if self.numWarmups != 0:
            command += (" --numWarmups=%s") % (self.numWarmups)
        if self.doFootprint:
            command += " --doFootprint"
        if self.doHistogram:
            command += " --doHistogram"
        if self.logDir is not None:
            command += " --outputLoc=%s" % (self.logDir)
        if self.waitTime is not None:
            command += " --waitTime=%s" % (self.waitTime)
        if self.runTime is not None:
            command += " --runTime=%s" % (self.runTime)
        if self.numRepeats is not None:
            command += " --numRepeats=%s" % (self.numRepeats)
        if self.perfConfig is not None:
            command += " --configLoc=%s" % (self.perfConfig)
        return command

    def getBenchmarkSpecificCommand(self, benchmark):
        command = ""
        return command

    def getBenchmarkTotalCommand(self,benchmark):
        benchmarkBase = self.getBaseCommand()
        benchmarkSpecific = self.getBenchmarkSpecificCommand(benchmark)
        command = "%s %s" % (benchmarkBase, benchmarkSpecific)
        return command

class FriendOfFriends(BaseBenchmarkFramework):
    def setInitialValues(self):
        self.configLocation = None
        self.javaLocation = EnvironmentVariables.javaLocation
        self.jarLocation = EnvironmentVariables.jarLocation

    def __init__(self):
        self.setInitialValues()

    def setupArgumentParser(self):
        parser = argparse.ArgumentParser(prog='Friend Of Friends Instrumentation')
        return parser

    def parseArgs(self):
        parser = self.setupArgumentParser()
        args, unknown = parser.parse_known_args()
        print "Friend Of Friends Framework unknown args: ", unknown

    def getBenchmarkList(self):
        benchmarks = [
            "fof"
        ]
        return benchmarks

    def getBaseCommand(self):
        command = self.javaLocation
        if self.configLocation is not None:
            command = "%s -Dconfig.file=%s" % (command, self.configLocation)
        command = "%s -jar %s/fof.jar" % (command, self.jarLocation)
        return command

    def getBenchmarkSpecificCommand(self, benchmark):
        command = ""
        return command

    def getBenchmarkTotalCommand(self,benchmark):
        benchmarkBase = self.getBaseCommand()
        benchmarkSpecific = self.getBenchmarkSpecificCommand(benchmark)
        command = "%s %s" % (benchmarkBase, benchmarkSpecific)
        return command
