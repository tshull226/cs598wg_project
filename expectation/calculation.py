import argparse
import re
import glob
import os
import sys
import numpy as np
import matplotlib.pyplot as plt


calc = 2.94e-10
mem = 9835.8e6


def performCalculation(n):
    result = 0.0
    two = n * n
    three = n * n * n
    six =  three * three
    comp_part = (18 * six - 3 * three)* calc
    mem_part = (16 * n + 48 * two + 146 * three) / mem
    result = comp_part + mem_part
    return result


def performCalculations():
    print "n, load, store, time"
    for i in range(2, 42, 2):
        value = i + 0.0
        time = performCalculation(value)
        store = storeFootprintEstimation(value)
        load = loadFootprintEstimation(value)
        print i , ", " , load , ", " , store , ", " , time



def storeFootprintEstimation(n):
    result = 0.0
    two = n * n
    three = n * n * n
    mem_part = (16 * two + 58 * three)
    result = mem_part
    return result

def loadFootprintEstimation(n):
    result = 0.0
    two = n * n
    three = n * n * n
    mem_part = (16 * n + 32 * two + 88 * three)
    result = mem_part
    return result

def main():
    performCalculations()



if __name__ == '__main__':
    main()
