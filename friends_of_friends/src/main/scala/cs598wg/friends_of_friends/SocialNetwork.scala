package cs598wg.friends_of_friends

import akka.actor.{Actor, ActorLogging, Props, ActorRef}

class SocialNetwork extends Actor with ActorLogging {
  import SocialNetwork._
  import User.RequestUserInfo
  import User.UserIdentity
  import User.RequestFriendsOfUserWithFriend

  /*
  val numUsers = 10
  for(i <- 1 to numUsers){
    context.actorOf(Props(new User()))
  }
  */

  var users = List[ActorRef]()

  def addFriendsToUser(userInfo: UserInfo, actor: ActorRef) = {
    //FIXME eventually need to have algorithm for this
    def shouldAddAsFriend() = {
      true
    }

    for(user <- users){
      if(shouldAddAsFriend()){
        user ! User.AddFriendFirst(userInfo, actor)
      }
    }
  }

  def receive = {
    case RequestUsers =>
      for(user <- users){
        user ! RequestUserInfo
      }

    case RequestFriendsofUsers =>
      sender() ! FriendsComputation.NumberOfResponsesExpected(users.size)
      for(user <- users){
        user ! User.RequestFriends(sender())
      }

    case UserIdentity(user) =>
      context.parent ! UserIdentity(user)

    case RequestFriendsOfUserorUsersFriends(identity) =>
      for(user <- users){
        user ! User.RequestFriendsIfRelated(identity, sender())
      }
      sender() ! FriendsComputation.NumberOfResponsesExpected(users.size)


    case RequestFriendsofUserswithFriend(userFriend) =>
      for(user <- users){
        user ! RequestFriendsOfUserWithFriend(userFriend, sender())
      }
      sender() ! FriendsComputation.NumberOfResponsesExpected(users.size)

    case AddUsers(userList) =>
      for(user <- userList){
        val ControllerActor.PersonInfo(name, dob, ssn) = user
        val userInfo = UserInfo(name, dob, ssn)
        val actor = context.actorOf(Props(new User(userInfo)))
        addFriendsToUser(userInfo, actor)
        users ::= actor
      }
    case PrintSocialNetworkInfo =>
      println(s"This is a social network with ${users.size} users")
      for(user <- users){
        user ! User.PrintUserInfo
      }
  }
}

object SocialNetwork {
  import ControllerActor.PersonInfo
    case class RequestFriendsofUserswithFriend(user: UserInfo)
    case class RequestFriendsOfUserorUsersFriends(user: UserInfo)
    case object RequestUsers
    case object RequestFriendsofUsers
    case object PrintSocialNetworkInfo
    case class AddUsers(users: List[PersonInfo])
}
