package cs598wg.friends_of_friends

import akka.actor.{Actor, ActorLogging, Props, ActorRef}

//These are temporary nodes to perform the necessary computation
class FriendsComputation(socialNetworks: List[ActorRef]) extends Actor with ActorLogging {
  import FriendsComputation._
  import User.FriendsOfUser
  import User.FriendsOfUserWithFriend

  //def retrieveResults(num: Int, personalFriends: Set[UserInfo], friendsOfFriends: Set[UserInfo]): Receive = {
  //  case FriendsOfUserWithFriend(_, _, friends) => 
  //    val nonPersonalFriends = friends.getOrElse(Set[UserInfo]()) -- personalFriends
  //    /*
  //    val nonPersonalFriends = friends match {
  //      case None =>
  //        Set[UserInfo]()
  //      case Some(friendSet) =>
  //        friendSet.filter(f => !(personalFriends contains f))
  //    }
  //    */
  //    context.become(retrieveResults(num + 1, personalFriends, friendsOfFriends ++ nonPersonalFriends))
  //}

  var numResponses = 0
  var numMessagesExpected = 0
  var numSocialNetworkResponses = 0
  var personalFriends = Set[UserInfo]()
  var friendsOfFriends = List[FriendsOfUserWithFriend]()

  def computeResult(identity: UserInfo) = {
    var isDone = false
    if(!checkForComplete()){
      println("not all computations have finished")
    }else{
      isDone = true
      println("all computations are done")
      val forbiddenFriends = personalFriends + identity
      var fof = Set[UserInfo]()
      for(friend <- friendsOfFriends){
        friend match {
          case FriendsOfUserWithFriend(_, _, None) =>
            ; //doing nothing
          case FriendsOfUserWithFriend(_, _, Some(friends)) =>
            //adding friends connected by degree of two
            fof ++= (friends -- forbiddenFriends)
        }
      }
      println(s"user ${identity.name} has ${fof.size} friends of friends")
      //stopping the actor (since its job has been completed)
      context.stop(self)
    }
    isDone
  }

  def checkForComplete() = {
    numSocialNetworkResponses == socialNetworks.size && numResponses == numMessagesExpected
  }

  def retrieveFriendsOfUser(identity: UserInfo): Receive = {
    case FriendsOfUser(user, friends) =>
      numResponses += 1
      personalFriends ++= friends
      computeResult(identity)
      context.become(retrieveFriendsOfUser(identity))
    case FriendsOfUserWithFriend(a,b,c) =>
      numResponses += 1
      friendsOfFriends ::= FriendsOfUserWithFriend(a,b,c)
      computeResult(identity)
      context.become(retrieveFriendsOfUser(identity))
    case NumberOfResponsesExpected(num) =>
      numSocialNetworkResponses += 1
      numMessagesExpected += num
      computeResult(identity)
  }

  def waitForRequest: Receive = {
    case ComputeFriendsOfFriends(user) =>
      //TODO want to request all friends here
      for (socialNetwork <- socialNetworks){
        socialNetwork ! SocialNetwork.RequestFriendsOfUserorUsersFriends(user)
      }
      context.become(retrieveFriendsOfUser(user))
  }

  def receive = waitForRequest
}

object FriendsComputation {
  val props = Props[FriendsComputation]
  case class ComputeFriendsOfFriends(user: UserInfo)
  case class NumberOfResponsesExpected(num: Int)
}
