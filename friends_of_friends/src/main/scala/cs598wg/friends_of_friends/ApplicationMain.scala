package cs598wg.friends_of_friends

import akka.actor.ActorSystem
import com.typesafe.config._

object ApplicationMain extends App {
  val config = ConfigFactory.load()
  println("config info: " + config.getValue("my.own.setting"))
  val system = ActorSystem("MyActorSystem")
  val controller = system.actorOf(ControllerActor.props, "controller")
  controller ! ControllerActor.PrintControllerInfo
  //really ugly hack for the time being
  Thread sleep 2000
  controller ! ControllerActor.PrintControllerInfo
  //controller ! ControllerActor.StartSimulation
  
  //FIXME need to get this to be the right thing
  var input = ""
  do{
    input = scala.io.StdIn.readLine("what do you want?\n")
    println(s"the input received is $input")
    input match {
      case "done" =>
        println("am finishing execution now")
      case "start" =>
        println("starting simulation now")
        controller ! ControllerActor.StartSimulation
      case "reset" =>
        println("resetting simulation")
        controller ! ControllerActor.ResetSimulation
      case _ =>
        println("didn't understand your input")
    }
  }while(input != "done");


  system.shutdown()
  system.awaitTermination()
}
