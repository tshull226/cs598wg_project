package cs598wg.friends_of_friends

import akka.actor.{Actor, ActorLogging, Props, ActorRef}

class ControllerActor extends Actor with ActorLogging {
  import ControllerActor._
  import User.UserIdentity
  import SocialNetwork.RequestUsers

  var socialNetworks = List[ActorRef]()
  //Creating the Social Networks
  def initializeSocialNetwork(){
    val numSocialNetworks = 2
    for(i <- 1 to numSocialNetworks){
      socialNetworks ::= context.actorOf(Props(new SocialNetwork()))
    }
  }

  //Creating the People I'm going to use and sending them to users
  def createPeopleAndPopulateNetworks(){
    var people = List[PersonInfo]()

    val numPeople = 3
    //FIXME need to make names and birthdays nicer
    for(i <- 1 to numPeople){
      val stringVal = "" + i
      people ::= PersonInfo(stringVal, stringVal, i)
    }

    //function for deciding how many of the users to add to the network
    def shouldAddUser() = {
      true
    }

    //now populating the social networks
    for(socialNetwork <- socialNetworks){
      //TODO need to decide how many people to add
      //also need to thing about how to make friends
      var userList = List[PersonInfo]()
      for(person <- people){
        if(shouldAddUser()){
          userList ::= person
        }
      }
      socialNetwork ! SocialNetwork.AddUsers(userList)
    }
  }

  initializeSocialNetwork()
  createPeopleAndPopulateNetworks()


  var usersSeen = Set[UserInfo]()
  def receive = {
    case StartSimulation =>
      log.info("In Controller - Starting Simulation")
      for(socialNetwork <- socialNetworks){
        socialNetwork ! RequestUsers
      }
    case ResetSimulation =>
      log.info("In Controller - Resetting Simulation")
      usersSeen = Set[UserInfo]()
    case UserIdentity(user) =>
      if(!(usersSeen contains user)){
        usersSeen = usersSeen + user
        println(s"saw user ${user.name}")
        val actor = context.actorOf(Props(new FriendsComputation(socialNetworks)))
        actor ! FriendsComputation.ComputeFriendsOfFriends(user)
      }
    case PrintControllerInfo =>
      println(s"this is a controller for ${socialNetworks.size} Social Networks")
      for(socialNetwork <- socialNetworks){
        socialNetwork ! SocialNetwork.PrintSocialNetworkInfo
      }
  }	
}

object ControllerActor {
  val props = Props[ControllerActor]
    case object StartSimulation
    case object ResetSimulation
    case object PrintControllerInfo
    case class PersonInfo(name: String, dob: String, ssn: Int)
}

