package cs598wg.friends_of_friends

//TODO need decide on how to initialize the network

import akka.actor.{Actor, ActorLogging, Props, ActorRef}

class User(identity: UserInfo) extends Actor with ActorLogging {
  import User._

  var friends = Set[UserInfo]()
  //val identity = UserInfo(name, dob, ssn)

  def receive = {
    case RequestUserInfo => 
      sender() ! UserIdentity(identity)

    case RequestFriends(computeNode) => 
      computeNode ! FriendsOfUser(identity, friends)

    case RequestFriendsOfUserWithFriend(friend, computeNode) => 
      if (friends contains friend){
        computeNode ! FriendsOfUserWithFriend(identity, friend, Some(friends))
      }
      else{
        computeNode ! FriendsOfUserWithFriend(identity, friend, None)
      }

    case RequestFriendsIfRelated(user, computeNode) =>
      if(identity == user){
        computeNode ! FriendsOfUser(user, friends)
      } else {
        if (friends contains user){
          computeNode ! FriendsOfUserWithFriend(identity, user, Some(friends))
        }
        else{
          computeNode ! FriendsOfUserWithFriend(identity, user, None)
        }
      }

    case AddFriendFirst(friend, actor) =>
      friends += friend
      actor ! AddFriendOther(identity)

    case AddFriendOther(friend) =>
      friends += friend

    case PrintUserInfo =>
      val UserInfo(name, dob, ssn) = identity
      println(s"This is $name. ssn: $ssn, dob: $dob. This person has ${friends.size} friends")
  }
}

//TODO need to change what messages here to what messages I can send (don't know who the receiver will be)
object User {
  case object RequestUserInfo
  case class RequestFriends(computeNode: ActorRef)
  case class AddFriendOther(user: UserInfo)
  case class AddFriendFirst(user: UserInfo, actor: ActorRef)
  case class RequestFriendsOfUserWithFriend(friend: UserInfo, computeNode: ActorRef)
  case class RequestFriendsIfRelated(user: UserInfo, computeNode: ActorRef)
  case class FriendsOfUser(user: UserInfo, friends: Set[UserInfo])
  case class FriendsOfUserWithFriend(user: UserInfo, userOfInterest: UserInfo, friends: Option[Set[UserInfo]])
  case class UserIdentity(user: UserInfo)
  case object PrintUserInfo
}

//TODO need to be able to match on the user info
  case class UserInfo(name: String, dob: String, ssn: Int)

