name := """friends_of_friends-app"""

version := "0.1"

scalaVersion := "2.11.6"

assemblyJarName in assembly := "friends_of_friends.jar"
assemblyOutputPath in assembly := file("../jars/friends_of_friends.jar")

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.3.11")
  //"com.typesafe.akka" %% "akka-testkit" % "2.3.11" % "test",
  //"org.scalatest" %% "scalatest" % "2.2.4" % "test")

connectInput in run := true

fork in run := true
